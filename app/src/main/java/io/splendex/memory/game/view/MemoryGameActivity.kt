package io.splendex.memory.game.view

import android.os.Bundle
import android.widget.BaseAdapter
import androidx.appcompat.app.AppCompatActivity
import io.splendex.memory.R
import io.splendex.memory.entitiy.Card
import io.splendex.memory.game.logic.adapter.CardAdapter
import kotlinx.android.synthetic.main.activity_memory_game.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class MemoryGameActivity : AppCompatActivity(), MemoryGameContract.MemoryGameViewContract {

    private val presenter: MemoryGameContract.MemoryGamePresenter by inject { parametersOf(this@MemoryGameActivity) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memory_game)

        presenter.onCreate(savedInstanceState)
    }

    override fun getContext(): AppCompatActivity {
        return this
    }

    override fun setAdapter(adapter: BaseAdapter) {
        gameGrid?.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        presenter.onStart()
    }

    override fun onStop() {
        super.onStop()
        presenter.onStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }
}