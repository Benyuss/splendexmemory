package io.splendex.memory.game.logic.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Toast
import io.splendex.memory.R
import io.splendex.memory.entitiy.Card
import io.splendex.memory.game.view.CardItemView


class CardAdapter(private val context: Context, private var dataSet: List<Card>) : BaseAdapter() {

    override fun getCount(): Int {
        return dataSet.size
    }

    override fun getItem(position: Int): Any? {
        return dataSet[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    // create a new ImageView for each item referenced by the Adapter
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val cardItemView: CardItemView
        var itemView: View? = convertView

        if (itemView == null) {

            // inflate the layout
            itemView = LayoutInflater.from(context).inflate(R.layout.item_card, parent, false)

            // well set up the ViewHolder
            cardItemView = CardItemView(itemView!!, context)

            // store the holder with the view.
            itemView.tag = cardItemView
        } else {
            // we've just avoided calling findViewById() on resource everytime
            // just use the viewHolder
            cardItemView = itemView.tag as CardItemView
        }

        cardItemView.initView(dataSet[position]) {
            //todo remove
            Toast.makeText(context, "clicked", Toast.LENGTH_SHORT).show()
        }

        return itemView
    }

    fun setDataSet(dataSet: List<Card>) {
        this.dataSet = dataSet
        notifyDataSetChanged()
    }

    fun flipBackSelectedCards(position1 : Int, position2 : Int) {

    }
}