package io.splendex.memory.game.logic

import io.splendex.memory.game.logic.adapter.CardAdapter
import io.splendex.memory.game.logic.vm.MemoryGameViewModel
import io.splendex.memory.game.logic.vm.MemoryGameViewState
import io.splendex.memory.game.view.MemoryGameContract
import io.splendex.memory.parent.BasePresenterImpl

class MemoryGamePresenterImpl(
    override val view: MemoryGameContract.MemoryGameViewContract,
    override val viewModel: MemoryGameViewModel
) : MemoryGameContract.MemoryGamePresenter, BasePresenterImpl<MemoryGameViewState>(view, viewModel) {

    private var adapter: CardAdapter? = null

    override fun doOnCreate() {
        adapter = CardAdapter(
            view.getContext(), arrayListOf()
        )
        view.setAdapter(adapter!!)
    }

    override fun doOnStart() {
        //todo board size
        viewModel.getBoardElements(20)
    }


    override fun handleViewState(state: MemoryGameViewState) {
        state.items?.let { items ->
            adapter?.setDataSet(items)

            if (state.selectedItemFirstPosition != null && state.selectedItemSecondPosition != null) {
                if (viewModel.checkMatch()) {
                    //todo block click listeners on selected items
                } else {
                    adapter?.flipBackSelectedCards(
                        state.selectedItemFirstPosition!!,
                        state.selectedItemSecondPosition!!
                    )
                }
                viewModel.resetSelections()
            }
        }
    }
}