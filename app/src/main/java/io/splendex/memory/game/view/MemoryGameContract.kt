package io.splendex.memory.game.view

import android.widget.BaseAdapter
import io.splendex.memory.game.logic.vm.MemoryGameViewState
import io.splendex.memory.parent.BasePresenter
import io.splendex.memory.parent.BaseViewContract

interface MemoryGameContract {

    interface MemoryGameViewContract : BaseViewContract{

        fun setAdapter(adapter: BaseAdapter)
    }

    interface MemoryGamePresenter : BasePresenter<MemoryGameViewState>
}