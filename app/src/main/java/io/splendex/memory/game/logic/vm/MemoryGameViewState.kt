package io.splendex.memory.game.logic.vm

import io.splendex.memory.entitiy.Card

data class MemoryGameViewState(
    var boardSize: Int = 0,
    var items: List<Card>? = null,
    var selectedItemFirstPosition: Int? = null,
    var selectedItemSecondPosition: Int? = null,
    var matches: Int = 0
)