package io.splendex.memory.game.view

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.bumptech.glide.Glide
import io.splendex.memory.entitiy.Card
import kotlinx.android.synthetic.main.item_card.view.*

class CardItemView @JvmOverloads constructor(
    private val preInflatedView: View? = null,
    context: Context,
    attributeSet: AttributeSet? = null
) : LinearLayout(context, attributeSet) {

    private var clickCallback: (() -> Unit)? = null

    init {
        //todo set background resource

        preInflatedView?.cardFrontContainer?.setOnClickListener {
            //todo animation, animation callback
            preInflatedView.cardFlipper?.flipTheView()
            clickCallback?.invoke()
        }
        preInflatedView?.cardBackContainer?.setOnClickListener {
            //todo animation, animation callback
            preInflatedView.cardFlipper?.flipTheView()
            clickCallback?.invoke()
        }
    }

    fun initView(card: Card, clickCallback: () -> Unit) {
        preInflatedView?.cardFrontImage?.run {
            Glide.with(context).load(card.imageUrl).into(this)
        }
        preInflatedView?.cardBackImage?.run {
            Glide.with(context).load("https://images.unsplash.com/photo-1536798227072-f571dcefdef1?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1057&q=80").into(this)
        }
        this.clickCallback = clickCallback
    }


}