package io.splendex.memory.game.logic.vm

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.splendex.memory.entitiy.Card
import io.splendex.memory.parent.BaseViewModel
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

class MemoryGameViewModel : BaseViewModel<MemoryGameViewState>() {

    init {
        viewState.value = MemoryGameViewState()
    }

    fun getBoardElements(boardSize: Int) {
        addDisposable(Observable.just(provideCards(boardSize)).observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                viewState.value = viewState.value?.copy(boardSize = boardSize, items = it)
            })
    }

    private fun provideCards(boardSize: Int): List<Card> {
        return arrayListOf(
            Card("https://picsum.photos/400/300", 1),
            Card("https://picsum.photos/400/300", 1),
            Card("https://picsum.photos/406/306", 2),
            Card("https://picsum.photos/406/306", 2),
            Card("https://picsum.photos/408/308", 3),
            Card("https://picsum.photos/408/308", 3),
            Card("https://picsum.photos/410/310", 4),
            Card("https://picsum.photos/410/310", 4),
            Card("https://picsum.photos/412/412", 5),
            Card("https://picsum.photos/414/414", 5),
            Card("https://picsum.photos/416/416", 6),
            Card("https://picsum.photos/398/298", 6),
            Card("https://picsum.photos/394/296", 7),
            Card("https://picsum.photos/394/296", 7),
            Card("https://picsum.photos/392/296", 8),
            Card("https://picsum.photos/392/296", 8),
            Card("https://picsum.photos/390/296", 9),
            Card("https://picsum.photos/390/296", 9),
            Card("https://picsum.photos/394/290", 10),
            Card("https://picsum.photos/394/290", 10)
        ).take(boardSize).shuffled(Random(Date().time))
    }

    fun selectFirstItem(position: Int) {

    }

    fun selectSecondItem(position: Int) {

    }

    fun checkMatch(): Boolean {
        var matchFound = false

        val state = viewState.value
        if (state?.items?.get(
                state.selectedItemFirstPosition ?: 0
            )
            == state?.items?.get(
                state.selectedItemSecondPosition ?: 0
            )
        ) {
            matchFound = true
        }

        return matchFound
    }

    fun resetGame() {
        viewState.value = viewState.value?.copy(
            items = provideCards(viewState.value?.boardSize ?: 0),
            selectedItemSecondPosition = null,
            selectedItemFirstPosition = null
        )
    }

    fun resetSelections() {
        viewState.value = viewState.value?.copy(selectedItemFirstPosition = null, selectedItemSecondPosition = null)
    }
}