package io.splendex.memory.parent

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseViewModel<T> : ViewModel() {

    open var viewState: MutableLiveData<T> = MutableLiveData()

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    protected fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        clearDisposables()
    }

    private fun clearDisposables() {
        compositeDisposable.clear()
    }
}