package io.splendex.memory.parent

import androidx.appcompat.app.AppCompatActivity

interface BaseViewContract {

    fun getContext(): AppCompatActivity
}