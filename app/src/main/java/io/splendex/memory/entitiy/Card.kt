package io.splendex.memory.entitiy

data class Card(
    var imageUrl: String,
    var typeId: Long
)