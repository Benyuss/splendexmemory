package io.splendex.memory.di

import io.splendex.memory.game.logic.MemoryGamePresenterImpl
import io.splendex.memory.game.logic.vm.MemoryGameViewModel
import io.splendex.memory.game.view.MemoryGameContract
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val memoryGameModule = module {
    viewModel { MemoryGameViewModel() }
    factory { (view: MemoryGameContract.MemoryGameViewContract) ->
        MemoryGamePresenterImpl(
            view,
            get()
        ) as MemoryGameContract.MemoryGamePresenter
    }
}